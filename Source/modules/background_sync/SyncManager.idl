// Copyright 2015 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

[
    Exposed=(Window,ServiceWorker),
    GarbageCollected,
    RuntimeEnabled=BackgroundSync,
    TypeChecking=Interface,
] interface SyncManager {
    [CallWith=ScriptState,ImplementedAs=registerFunction] Promise<SyncRegistration> register(optional SyncRegistrationOptions options);
    [CallWith=ScriptState] Promise<SyncRegistration> getRegistration(DOMString id);
    [CallWith=ScriptState] Promise<sequence<SyncRegistration>> getRegistrations();
    readonly attribute unsigned long minAllowablePeriod;
};
