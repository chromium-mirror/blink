layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutSVGRoot {svg} at (10,0) size 445x120
    LayoutSVGHiddenContainer {defs} at (0,0) size 0x0
      LayoutSVGResourceLinearGradient {linearGradient} [id="linGrad"] [gradientUnits=objectBoundingBox] [start=(0,0)] [end=(1,1)]
        LayoutSVGGradientStop {stop} [offset=0.00] [color=#FF0000]
        LayoutSVGGradientStop {stop} [offset=1.00] [color=#00FF00]
      LayoutSVGResourceRadialGradient {radialGradient} [id="radGrad"] [gradientUnits=objectBoundingBox] [center=(0.50,0.50)] [focal=(0.50,0.50)] [radius=0.50] [focalRadius=0.00]
        LayoutSVGGradientStop {stop} [offset=0.00] [color=#FF0000]
        LayoutSVGGradientStop {stop} [offset=1.00] [color=#00FF00]
      LayoutSVGResourceFilter {filter} [id="f1"] [filterUnits=objectBoundingBox] [primitiveUnits=objectBoundingBox]
        [feDisplacementMap scale="64.00" xChannelSelector="RED" yChannelSelector="GREEN"]
          [feTile]
            [feComposite operation="OVER"]
              [feFlood flood-color="#000000" flood-opacity="1.00"]
              [feFlood flood-color="#000000" flood-opacity="1.00"]
          [SourceGraphic]
      LayoutSVGResourceFilter {filter} [id="over"] [filterUnits=objectBoundingBox] [primitiveUnits=objectBoundingBox]
        [feComposite operation="OVER"]
          [feFlood flood-color="#0000FF" flood-opacity="0.50"]
          [feFlood flood-color="#FF0000" flood-opacity="0.50"]
      LayoutSVGResourceFilter {filter} [id="gauss"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
        [feGaussianBlur stdDeviation="10.00, 10.00"]
          [SourceGraphic]
      LayoutSVGResourceFilter {filter} [id="dilate2"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
        [feMorphology operator="DILATE" radius="3.00, 1.00"]
          [SourceGraphic]
    LayoutSVGImage {image} at (10,10) size 100x100
      [filter="f1"] LayoutSVGResourceFilter {filter} at (10,10) size 100x100
    LayoutSVGRect {rect} at (130,10) size 100x100 [fill={[type=SOLID] [color=#000000]}] [x=130.00] [y=10.00] [width=100.00] [height=100.00]
      [filter="over"] LayoutSVGResourceFilter {filter} at (130,10) size 100x100
    LayoutSVGContainer {g} at (240,0) size 120x120 [transform={m=((1.00,0.00)(0.00,1.00)) t=(250.00,10.00)}]
      [filter="gauss"] LayoutSVGResourceFilter {filter} at (-10,-10) size 120x120
      LayoutSVGRect {rect} at (250,10) size 75x75 [fill={[type=SOLID] [color=#FF0000]}] [x=0.00] [y=0.00] [width=75.00] [height=75.00]
      LayoutSVGRect {rect} at (275,35) size 75x75 [fill={[type=SOLID] [color=#0000FF]}] [x=25.00] [y=25.00] [width=75.00] [height=75.00]
    LayoutSVGContainer {g} at (390,30) size 65x65 [transform={m=((1.00,0.00)(0.00,1.00)) t=(370.00,10.00)}]
      [filter="dilate2"] LayoutSVGResourceFilter {filter} at (20,20) size 65x65
      LayoutSVGRect {rect} at (389,29) size 52x52 [stroke={[type=SOLID] [color=#FF0000]}] [x=20.00] [y=20.00] [width=50.00] [height=50.00]
      LayoutSVGRect {rect} at (404,44) size 52x52 [stroke={[type=SOLID] [color=#0000FF]}] [x=35.00] [y=35.00] [width=50.00] [height=50.00]
