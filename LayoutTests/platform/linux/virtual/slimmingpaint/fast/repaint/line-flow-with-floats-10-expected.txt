{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "repaintRects": [
        [368, 440, 1, 59],
        [14, 374, 48, 64],
        [14, 363, 48, 64],
        [8, 360, 418, 139]
      ],
      "paintInvalidationClients": [
        "InlineTextBox 'was a very\n'",
        "InlineTextBox 'difficult game indeed.\n'",
        "RootInlineBox",
        "InlineTextBox 'The players all played at once without waiting\n'",
        "InlineTextBox 'for'",
        "RootInlineBox",
        "InlineTextBox 'turns,\n'",
        "InlineTextBox 'quarrelling all the while, and fighting for the'",
        "RootInlineBox",
        "InlineTextBox 'hedgehogs; and in\n'",
        "InlineTextBox 'a very short time '",
        "InlineTextBox 'the Queen'",
        "RootInlineBox",
        "InlineTextBox 'was in a furious passion, and went\n'",
        "InlineTextBox 'stamping about, and'",
        "RootInlineBox",
        "InlineTextBox 'shouting \u2018Off with his head!\u2019 or \u2018Off with\n'",
        "InlineTextBox 'her head!\u2019 about'",
        "RootInlineBox",
        "InlineTextBox 'once in a minute.\n'",
        "RootInlineBox",
        "LayoutBlockFlow P",
        "LayoutBlockFlow (floating) SPAN id='blueFloat'",
        "LayoutText #text",
        "InlineTextBox 'was in a furious passion, and went\n'",
        "InlineTextBox 'stamping'",
        "InlineTextBox 'about, and shouting \u2018Off with his head!\u2019 or \u2018Off with\n'",
        "InlineTextBox 'her'",
        "InlineTextBox 'head!\u2019 about once in a minute.\n'"
      ]
    }
  ]
}

