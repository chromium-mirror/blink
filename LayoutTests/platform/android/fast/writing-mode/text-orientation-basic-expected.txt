layer at (0,0) size 800x712
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x712
  LayoutBlockFlow {HTML} at (0,0) size 800x712
    LayoutBlockFlow {BODY} at (8,8) size 784x696
      LayoutBlockFlow {P} at (0,0) size 784x80
        LayoutText {#text} at (0,0) size 762x39
          text run at (0,0) width 336: "This test shows the following bugs with text-orientation. "
          text run at (336,0) width 426: "The far right test mis-renders differently in both the simple and complex"
          text run at (0,20) width 319: "text paths. The simple text path mis-renders because "
        LayoutInline {TT} at (0,0) size 304x16
          LayoutText {#text} at (319,23) size 304x16
            text run at (319,23) width 304: "CTFontGetVerticalTranslationsForGlyphs"
        LayoutText {#text} at (623,20) size 774x59
          text run at (623,20) width 143: " returns incorrect values"
          text run at (0,40) width 313: "for horizontal glyphs that have vertical counterparts. "
          text run at (313,40) width 413: "The complex text code path mis-renders because of API deficiency. "
          text run at (726,40) width 48: "There is"
          text run at (0,60) width 514: "no way to tell CoreText to use upright horizontal glyphs when rendering a vertical line."
      LayoutBlockFlow {DIV} at (0,96) size 348x600
        LayoutBlockFlow {DIV} at (0,0) size 174x421 [border: (1px solid #008000)]
          LayoutInline {SPAN} at (0,0) size 41x170
            LayoutText {#text} at (2,1) size 41x170
              text run at (2,1) width 170: "Hello world"
          LayoutBR {BR} at (11,171) size 0x0
          LayoutInline {SPAN} at (0,0) size 41x419
            LayoutText {#text} at (45,1) size 41x419
              text run at (45,1) width 419: "Hello world"
          LayoutBR {BR} at (54,420) size 0x0
          LayoutInline {SPAN} at (0,0) size 41x170
            LayoutText {#text} at (88,1) size 41x170
              text run at (88,1) width 170: "Hello world"
          LayoutBR {BR} at (97,171) size 0x0
          LayoutInline {SPAN} at (0,0) size 41x419
            LayoutText {#text} at (131,1) size 41x419
              text run at (131,1) width 419: "Hello world"
          LayoutBR {BR} at (140,420) size 0x0
        LayoutText {#text} at (0,0) size 0x0
        LayoutBlockFlow {DIV} at (174,0) size 174x172 [border: (1px solid #008000)]
          LayoutInline {SPAN} at (0,0) size 41x170
            LayoutText {#text} at (2,1) size 41x170
              text run at (2,1) width 170: "Hello world"
          LayoutBR {BR} at (11,171) size 0x0
          LayoutInline {SPAN} at (0,0) size 41x170
            LayoutText {#text} at (45,1) size 41x170
              text run at (45,1) width 170: "Hello world"
          LayoutBR {BR} at (54,171) size 0x0
          LayoutInline {SPAN} at (0,0) size 41x170
            LayoutText {#text} at (88,1) size 41x170
              text run at (88,1) width 170: "Hello world"
          LayoutBR {BR} at (97,171) size 0x0
          LayoutInline {SPAN} at (0,0) size 41x170
            LayoutText {#text} at (131,1) size 41x170
              text run at (131,1) width 170: "Hello world"
          LayoutBR {BR} at (140,171) size 0x0
        LayoutText {#text} at (0,0) size 0x0
