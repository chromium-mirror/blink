layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (8,8) size 784x576
      LayoutBlockFlow {P} at (0,0) size 784x18
        LayoutText {#text} at (0,0) size 521x17
          text run at (0,0) width 521: "Tests that CSS-generated :before or :after content doesn't intrude into ruby bases."
      LayoutBlockFlow {P} at (0,34) size 784x0
      LayoutBlockFlow (anonymous) at (0,34) size 784x30
        LayoutText {#text} at (0,12) size 29x17
          text run at (0,12) width 29: "Foo "
        LayoutRuby (inline) {RUBY} at (0,0) size 79x17
          LayoutInline {<pseudo:before>} at (0,0) size 53x17
            LayoutTextFragment at (28,12) size 53x17
              text run at (28,12) width 53: "[before]"
          LayoutRubyRun (anonymous) at (76.41,12) size 34x18
            LayoutRubyText {RT} at (0,-12) size 34x12
              LayoutText {#text} at (0,0) size 34x12
                text run at (0,0) width 34: "long text"
            LayoutRubyBase (anonymous) at (0,0) size 34x18
              LayoutText {#text} at (13,0) size 8x17
                text run at (13,0) width 8: "b"
        LayoutText {#text} at (106,12) size 28x17
          text run at (106,12) width 28: " Bar"
        LayoutBR {BR} at (133,26) size 1x0
      LayoutBlockFlow {P} at (0,80) size 784x0
      LayoutBlockFlow {P} at (0,80) size 784x0
      LayoutBlockFlow (anonymous) at (0,80) size 784x30
        LayoutText {#text} at (0,12) size 29x17
          text run at (0,12) width 29: "Foo "
        LayoutRuby (inline) {RUBY} at (0,0) size 67x17
          LayoutRubyRun (anonymous) at (24.89,12) size 34x18
            LayoutRubyText {RT} at (0,-12) size 34x12
              LayoutText {#text} at (0,0) size 34x12
                text run at (0,0) width 34: "long text"
            LayoutRubyBase (anonymous) at (0,0) size 34x18
              LayoutText {#text} at (13,0) size 8x17
                text run at (13,0) width 8: "b"
          LayoutInline {<pseudo:after>} at (0,0) size 41x17
            LayoutTextFragment at (54,12) size 41x17
              text run at (54,12) width 41: "[after]"
        LayoutText {#text} at (94,12) size 28x17
          text run at (94,12) width 28: " Bar"
        LayoutBR {BR} at (121,26) size 1x0
      LayoutBlockFlow {P} at (0,126) size 784x0
      LayoutBlockFlow {P} at (0,126) size 784x0
      LayoutBlockFlow (anonymous) at (0,126) size 784x30
        LayoutText {#text} at (0,12) size 29x17
          text run at (0,12) width 29: "Foo "
        LayoutRuby (inline) {RUBY} at (0,0) size 119x17
          LayoutInline {<pseudo:before>} at (0,0) size 53x17
            LayoutTextFragment at (28,12) size 53x17
              text run at (28,12) width 53: "[before]"
          LayoutRubyRun (anonymous) at (76.41,12) size 34x18
            LayoutRubyText {RT} at (0,-12) size 34x12
              LayoutText {#text} at (0,0) size 34x12
                text run at (0,0) width 34: "long text"
            LayoutRubyBase (anonymous) at (0,0) size 34x18
              LayoutText {#text} at (13,0) size 8x17
                text run at (13,0) width 8: "b"
          LayoutInline {<pseudo:after>} at (0,0) size 41x17
            LayoutTextFragment at (106,12) size 41x17
              text run at (106,12) width 41: "[after]"
        LayoutText {#text} at (146,12) size 28x17
          text run at (146,12) width 28: " Bar"
        LayoutBR {BR} at (173,26) size 1x0
      LayoutBlockFlow {P} at (0,172) size 784x0
