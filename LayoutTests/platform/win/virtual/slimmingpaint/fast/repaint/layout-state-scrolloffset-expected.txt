{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "repaintRects": [
        [130, 118, 11, 17],
        [100, 118, 50, 17]
      ],
      "paintInvalidationClients": [
        "InlineTextBox ''",
        "RootInlineBox",
        "LayoutBlockFlow DIV id='target'",
        "LayoutText #text",
        "InlineTextBox 'after'"
      ]
    }
  ]
}

