layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (8,8) size 784x584
      LayoutBlockFlow {P} at (0,0) size 784x36
        LayoutText {#text} at (0,0) size 55x18
          text run at (0,0) width 55: "Test for "
        LayoutInline {I} at (0,0) size 667x36
          LayoutInline {A} at (0,0) size 306x18 [color=#0000EE]
            LayoutText {#text} at (54,0) size 306x18
              text run at (54,0) width 306: "http://bugs.webkit.org/show_bug.cgi?id=14221"
          LayoutText {#text} at (359,0) size 667x36
            text run at (359,0) width 5: " "
            text run at (363,0) width 304: "Repro crash (ASSERTION FAILED: oldText in"
            text run at (0,18) width 609: "LayoutBlockFlow::updateFirstLetter() during relayout of :before content with first-letter style)"
        LayoutText {#text} at (608,18) size 5x18
          text run at (608,18) width 5: "."
      LayoutBlockFlow {DIV} at (0,52) size 784x33
        LayoutInline {<pseudo:before>} at (0,0) size 47x28
          LayoutInline {<pseudo:first-letter>} at (0,0) size 18x33 [color=#008000]
            LayoutTextFragment at (0,0) size 18x33
              text run at (0,0) width 18: "T"
          LayoutTextFragment at (17,4) size 30x28
            text run at (17,4) width 30: "he "
        LayoutText {#text} at (46,4) size 408x28
          text run at (46,4) width 408: "first letter is green and larger than the rest."
