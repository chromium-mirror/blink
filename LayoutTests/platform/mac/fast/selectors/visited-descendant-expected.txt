layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (8,8) size 784x576
      LayoutBlockFlow {P} at (0,0) size 784x18
        LayoutText {#text} at (0,0) size 778x18
          text run at (0,0) width 778: "Test that visited style matches to the topmost link in a decendant selector. The link should be green, with red underlining."
      LayoutBlockFlow {P} at (0,34) size 784x18
        LayoutInline {A} at (0,0) size 31x18 [color=#FF0000]
          LayoutInline {SPAN} at (0,0) size 31x18 [color=#008000]
            LayoutText {#text} at (0,0) size 31x18
              text run at (0,0) width 31: "Link"
        LayoutText {#text} at (0,0) size 0x0
      LayoutBlockFlow {P} at (0,68) size 784x18
        LayoutText {#text} at (0,0) size 745x18
          text run at (0,0) width 745: "Test that visited style matches to the topmost link in a child selector. The link should be green, with red underlining."
      LayoutBlockFlow {P} at (0,102) size 784x18
        LayoutInline {A} at (0,0) size 31x18 [color=#FF0000]
          LayoutInline {SPAN} at (0,0) size 31x18 [color=#008000]
            LayoutText {#text} at (0,0) size 31x18
              text run at (0,0) width 31: "Link"
        LayoutText {#text} at (0,0) size 0x0
      LayoutBlockFlow {P} at (0,136) size 784x18
        LayoutText {#text} at (0,0) size 669x18
          text run at (0,0) width 669: "Test that visited style does not match to non-topmost links. The link should be red, with red underlining."
      LayoutBlockFlow {P} at (0,170) size 784x18
        LayoutInline {A} at (0,0) size 0x0 [color=#FF0000]
          LayoutInline {SPAN} at (0,0) size 0x0
        LayoutInline {A} at (0,0) size 31x18 [color=#FF0000]
          LayoutText {#text} at (0,0) size 31x18
            text run at (0,0) width 31: "Link"
        LayoutText {#text} at (0,0) size 0x0
      LayoutBlockFlow {P} at (0,204) size 784x36
        LayoutText {#text} at (0,0) size 784x36
          text run at (0,0) width 784: "Test that direct adjacent selector doesn't match visited style. The link should be red, with red underlining. The span should"
          text run at (0,18) width 59: "be green."
      LayoutBlockFlow {P} at (0,256) size 784x18
        LayoutInline {A} at (0,0) size 31x18 [color=#FF0000]
          LayoutText {#text} at (0,0) size 31x18
            text run at (0,0) width 31: "Link"
        LayoutText {#text} at (30,0) size 5x18
          text run at (30,0) width 5: " "
        LayoutInline {SPAN} at (0,0) size 33x18 [color=#008000]
          LayoutText {#text} at (34,0) size 33x18
            text run at (34,0) width 33: "Span"
        LayoutText {#text} at (0,0) size 0x0
      LayoutBlockFlow {P} at (0,290) size 784x36
        LayoutText {#text} at (0,0) size 749x36
          text run at (0,0) width 749: "Test that indirect adjacent selector doesn't match visited style. The link should be red, with red underlining. The span"
          text run at (0,18) width 106: "should be green."
      LayoutBlockFlow {P} at (0,342) size 784x18
        LayoutInline {A} at (0,0) size 31x18 [color=#FF0000]
          LayoutText {#text} at (0,0) size 31x18
            text run at (0,0) width 31: "Link"
        LayoutText {#text} at (30,0) size 5x18
          text run at (30,0) width 5: " "
        LayoutInline {SPAN} at (0,0) size 33x18 [color=#008000]
          LayoutText {#text} at (34,0) size 33x18
            text run at (34,0) width 33: "Span"
        LayoutText {#text} at (0,0) size 0x0
