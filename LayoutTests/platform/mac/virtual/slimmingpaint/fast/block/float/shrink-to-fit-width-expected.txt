layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (8,8) size 784x584
      LayoutBlockFlow {P} at (0,0) size 784x18
        LayoutText {#text} at (0,0) size 321x18
          text run at (0,0) width 321: "Test shrink-to-fit width for floating elements (see "
        LayoutInline {A} at (0,0) size 97x18 [color=#0000EE]
          LayoutText {#text} at (320,0) size 97x18
            text run at (320,0) width 97: "CSS 2.1 10.3.5"
        LayoutText {#text} at (416,0) size 11x18
          text run at (416,0) width 11: ")."
      LayoutBlockFlow {P} at (0,34) size 784x36
        LayoutText {#text} at (0,0) size 203x18
          text run at (0,0) width 203: "Shrink-to-fit width is min(max("
        LayoutInline {I} at (0,0) size 164x18
          LayoutText {#text} at (202,0) size 164x18
            text run at (202,0) width 164: "preferred minimum width"
        LayoutText {#text} at (365,0) size 9x18
          text run at (365,0) width 9: ", "
        LayoutInline {I} at (0,0) size 100x18
          LayoutText {#text} at (373,0) size 100x18
            text run at (373,0) width 100: "available width"
        LayoutText {#text} at (472,0) size 15x18
          text run at (472,0) width 15: "), "
        LayoutInline {I} at (0,0) size 101x18
          LayoutText {#text} at (486,0) size 101x18
            text run at (486,0) width 101: "preferred width"
        LayoutText {#text} at (586,0) size 741x36
          text run at (586,0) width 14: "). "
          text run at (599,0) width 142: "In the following cases"
          text run at (0,18) width 111: "(except the \x{201C}x\x{201D}), "
        LayoutInline {I} at (0,0) size 164x18
          LayoutText {#text} at (110,18) size 164x18
            text run at (110,18) width 164: "preferred minimum width"
        LayoutText {#text} at (273,18) size 18x18
          text run at (273,18) width 18: " < "
        LayoutInline {I} at (0,0) size 100x18
          LayoutText {#text} at (290,18) size 100x18
            text run at (290,18) width 100: "available width"
        LayoutText {#text} at (389,18) size 18x18
          text run at (389,18) width 18: " < "
        LayoutInline {I} at (0,0) size 101x18
          LayoutText {#text} at (406,18) size 101x18
            text run at (406,18) width 101: "preferred width"
        LayoutText {#text} at (0,0) size 0x0
      LayoutBlockFlow {DIV} at (0,86) size 100x236
        LayoutBlockFlow (floating) {DIV} at (0,0) size 100x42 [border: (3px solid #000000)]
          LayoutText {#text} at (3,3) size 62x36
            text run at (3,3) width 62: "longword"
            text run at (3,21) width 62: "longword"
        LayoutBlockFlow {DIV} at (0,42) size 100x10
        LayoutBlockFlow (floating) {DIV} at (0,52) size 100x42 [border: (3px solid #000000)]
          LayoutText {#text} at (3,3) size 62x36
            text run at (3,3) width 62: "longword"
            text run at (3,21) width 62: "longword"
        LayoutBlockFlow {DIV} at (0,94) size 100x10
        LayoutBlockFlow (floating) {DIV} at (0,104) size 100x42 [border: (3px solid #000000)]
          LayoutText {#text} at (3,3) size 62x36
            text run at (3,3) width 62: "longword"
            text run at (3,21) width 62: "longword"
        LayoutBlockFlow (floating) {DIV} at (86,146) size 14x24 [border: (3px solid #000000)]
          LayoutText {#text} at (3,3) size 8x18
            text run at (3,3) width 8: "x"
        LayoutBlockFlow {DIV} at (0,170) size 100x10
        LayoutBlockFlow (floating) {DIV} at (0,180) size 100x46 [border: (3px solid #000000)]
          LayoutBlockFlow {DIV} at (3,3) size 60x20 [bgcolor=#C0C0C0]
          LayoutText {#text} at (0,0) size 0x0
          LayoutBlockFlow {DIV} at (3,23) size 60x20 [bgcolor=#C0C0C0]
          LayoutText {#text} at (0,0) size 0x0
        LayoutBlockFlow {DIV} at (0,226) size 100x10
        LayoutBlockFlow (floating) {DIV} at (0,236) size 100x46 [border: (3px solid #000000)]
          LayoutBlockFlow {DIV} at (3,3) size 60x20 [bgcolor=#C0C0C0]
          LayoutText {#text} at (0,0) size 0x0
          LayoutBlockFlow {DIV} at (3,23) size 60x20 [bgcolor=#C0C0C0]
          LayoutText {#text} at (0,0) size 0x0
