Cross-origin access to 'window.location.pathname' over 'get' and 'set' in property descriptor should throw a SecurityError.

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS pathnameDescriptor is non-null.
PASS typeof pathnameDescriptor.get is "function"
PASS typeof pathnameDescriptor.set is "function"
PASS pathnameDescriptor.get.call(targetWindow.location) threw exception SecurityError: Blocked a frame with origin "http://127.0.0.1:8000" from accessing a cross-origin frame..
PASS pathnameDescriptor.set.call(targetWindow.location, 1) threw exception SecurityError: Blocked a frame with origin "http://127.0.0.1:8000" from accessing a cross-origin frame..
PASS successfullyParsed is true

TEST COMPLETE

