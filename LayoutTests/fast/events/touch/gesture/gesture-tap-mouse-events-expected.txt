Verifies that tapping on an element sends mouse events to appropriate ancestors of the tapped element in correct order.

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


--- Tap-only tests ---
-- sending gestureTap to green --
green received mouseover
grey received mouseenter
PASS wasInside['grey'] is false
lightgreen received mouseenter
PASS wasInside['lightgreen'] is false
green received mouseenter
PASS wasInside['green'] is false
green received mousemove
-- sending gestureTap to blue --
green received mouseout
blue received mouseover
green received mouseleave
PASS wasInside['green'] is true
lightgreen received mouseleave
PASS wasInside['lightgreen'] is true
lightblue received mouseenter
PASS wasInside['lightblue'] is false
blue received mouseenter
PASS wasInside['blue'] is false
blue received mousemove
-- sending gestureTap to blue --
blue received mousemove
-- sending gestureTap to lightblue --
blue received mouseout
lightblue received mouseover
blue received mouseleave
PASS wasInside['blue'] is true
lightblue received mousemove
--- Interleaved tap/mouse tests ---
-- sending mouseMove to lightgreen --
lightblue received mouseout
lightgreen received mouseover
lightblue received mouseleave
PASS wasInside['lightblue'] is true
lightgreen received mouseenter
PASS wasInside['lightgreen'] is false
lightgreen received mousemove
-- sending gestureTap to lightblue --
lightgreen received mouseout
lightblue received mouseover
lightgreen received mouseleave
PASS wasInside['lightgreen'] is true
lightblue received mouseenter
PASS wasInside['lightblue'] is false
lightblue received mousemove
-- sending mouseMove to lightgreen --
lightblue received mouseout
lightgreen received mouseover
lightblue received mouseleave
PASS wasInside['lightblue'] is true
lightgreen received mouseenter
PASS wasInside['lightgreen'] is false
lightgreen received mousemove
-- sending mouseMove to lightgreen --
lightgreen received mousemove
PASS successfullyParsed is true

TEST COMPLETE

